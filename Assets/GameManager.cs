﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public int dialogueInstance;
    public Fungus.Flowchart flowChart;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }

        else
        {
            Destroy(this);
        }
    }

    public void OnNextDialogue()
    {
        dialogueInstance++;

        if(dialogueInstance == 1)
        {
            flowChart.ExecuteBlock("Charles02");
        }

        else if (dialogueInstance == 2)
        {
            flowChart.ExecuteBlock("Charles03");
        }

        else if (dialogueInstance == 3)
        {
            flowChart.ExecuteBlock("Charles04");
        }

        else if (dialogueInstance == 4)
        {
            flowChart.ExecuteBlock("Dolores02");
        }

        else if (dialogueInstance == 5)
        {
            flowChart.ExecuteBlock("Dolores03");
        }

        else if (dialogueInstance == 6)
        {
            flowChart.ExecuteBlock("Dolores04");
        }

        else if (dialogueInstance == 7)
        {
            flowChart.ExecuteBlock("Jack02");
        }

        else if (dialogueInstance == 8)
        {
            flowChart.ExecuteBlock("Jack03");
        }

        else if (dialogueInstance == 9)
        {
            flowChart.ExecuteBlock("Jack04");
        }
    }

    public void OnGameOver()
    {
        flowChart.ExecuteBlock("Gameover_transition");
    }
}
