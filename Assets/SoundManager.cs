﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    public static SoundManager _instance;
    public List<AudioSource> allSources;
    public int goodSource;
    public GameObject mainMusic;


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }

        else
        {
            Destroy(this);
        }
    }
    public void Start()
    {
        allSources = new List<AudioSource>();
        for (int i = 0; i < DialogueManager._instance.son.Length; i++)
        {
            AudioSource tempSource = this.gameObject.AddComponent<AudioSource>();
            tempSource.loop = true;
            tempSource.clip = DialogueManager._instance.son[i];
            tempSource.volume = 0f;
            allSources.Add(tempSource);
        }

        for (int i = 0; i < allSources.Count; i++)
        {
            allSources[i].Play();
        }
    }

    public void PlaySound(int posList, bool isGood)
    {
        for (int i = 0; i < allSources.Count; i++)
        {
            allSources[i].volume = 0f;
        }

        if(isGood)
        {
            mainMusic.GetComponent<AudioSource>().volume = 0f;
        }

        else if(!isGood)
        {
            if(DialogueManager._instance.dialogueInstance == 0)
            {
                mainMusic.GetComponent<AudioSource>().volume = 100f;
            }

            else
            {
                mainMusic.GetComponent<AudioSource>().volume = 0f;
            }

            if (goodSource != 0)
            {
                allSources[goodSource].volume = 100f;
            }
        }

        allSources[posList].volume = 100f;
    }

    public void StopSound(int posList)
    {
        //if (isGood)
        //{
        //    //mainMusic.GetComponent<AudioSource>().volume = 100f;
        //}

        //else
        //{
        //    if(DialogueManager._instance.dialogueInstance != 0)
        //    {
        //        mainMusic.GetComponent<AudioSource>().volume = 100f;
        //    }

        //    else
        //    {
        //        mainMusic.GetComponent<AudioSource>().volume = 0f;
        //    }
        //}

        allSources[posList].volume = 0f;

        if(mainMusic.GetComponent<AudioSource>().volume == 0f && GameManager._instance.dialogueInstance == 0)
        {
            mainMusic.GetComponent<AudioSource>().volume = 100f;
        }

        if(goodSource != 0)
        {
            allSources[goodSource].volume = 100f;
        }
    }
}
