﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Canvas credits;
    public Canvas menu;

    private void Start()
    {
        credits.gameObject.SetActive(false);
        menu.gameObject.SetActive(true);
    }

    public void OnStartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void OnCredits()
    {
        menu.gameObject.SetActive(false);
        credits.gameObject.SetActive(true);
    }

    public void OnQuitGame()
    {
        Application.Quit();
    }

    public void OnGoBack()
    {
        if (SceneManager.GetActiveScene().name == "Menu")
        {
            SceneManager.LoadScene("Menu");
        }

        else
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
