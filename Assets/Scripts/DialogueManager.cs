﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager _instance;
    public int dialogueInstance;

    public string[] bText;
    public List<GameObject> buttonList;

    public AudioClip[] son;
    public bool[] goodAnswer;
    public string[] textButton;
    public Canvas can;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }

        else
        {
            Destroy(this);
        }
    }

    public void SetButtonsInactive()
    {
        foreach (GameObject go in buttonList)
        {
            go.SetActive(false);
        }
    }
}
