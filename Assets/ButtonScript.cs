﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    public AudioClip son;
    public bool goodAns;
    public string text;
    public int number;
    public bool isOver;
    Canvas can;

    private void Awake()
    {
        this.name = ("button " + DialogueManager._instance.buttonList.Count); 
        DialogueManager._instance.buttonList.Add(this.gameObject);
        number = DialogueManager._instance.buttonList.IndexOf(this.gameObject);
        son = DialogueManager._instance.son[number];
        goodAns = DialogueManager._instance.goodAnswer[number];
        text = DialogueManager._instance.textButton[number];
        GetComponentInChildren<Text>().text = text;
        can = DialogueManager._instance.can;
        gameObject.transform.SetParent(can.transform, true);
        gameObject.transform.localScale = new Vector3(1, 1, 1);
    }

    private void OnMouseOver()
    {
        SoundManager._instance.PlaySound(number, goodAns);
        isOver = true;
    }

    private void OnMouseExit()
    {
        SoundManager._instance.StopSound(number);
        isOver = false;
    }

    private void OnMouseDown()
    {
        if(isOver)
        {
            CheckAnswer();
        }
    }

    public void CheckAnswer()
    {
        if(goodAns == true)
        {
            DialogueManager._instance.SetButtonsInactive();
            GameManager._instance.OnNextDialogue();
            SoundManager._instance.PlaySound(number, goodAns);
            SoundManager._instance.goodSource = number;
        }

        else
        {
            GameManager._instance.OnGameOver();
        }
    }
}
